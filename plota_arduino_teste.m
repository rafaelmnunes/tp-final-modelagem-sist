t = zeros(1,8000);
temp = nan*zeros(1,8000);
pwm = nan*zeros(1,8000);
dados  = zeros(3,80000);

for i = 1:size(dados,2)
    t = wshift(1,t,1);
    temp = wshift(1,temp,1);
    pwm = wshift(1,pwm,1);
    sp = wshift(1,sp,1);
    aux = fscanf(s,'%f %f %f %i');
    t(end) = aux(1);
    temp(end) = aux(2);
    pwm(end) = aux(3)/1023;
    figure(1);
    subplot(2,1,1), plot(t,temp,'b');
    subplot(2,1,2), plot(t,pwm);
    drawnow;
    dados(1,i) = t(end);
    dados(2,i) = temp(end);
    dados(3,i) = pwm(end);
    if mod(i,240) == 0
        save dados_teste_peltier_agua2.mat dados;
    end
end