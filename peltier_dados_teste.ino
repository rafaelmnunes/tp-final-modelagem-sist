#include<OneWire.h>

//definições pro código ficar um pouco mais legível
#define PWM_OUT_PIN 9 //pino de saída que estamos utilizando
#define pwm OCR1A //Nosso limite do PWM deve ser escrito no registrador OCR1A

#define RANDOM_PIN 0 //pino utilizado para inicializar a semente do gerador de números aleatórios

//declarações relativas ao sensor de temperatura
#define TEMP_PIN 3 //pino do sensor de temperatura
OneWire sensor(TEMP_PIN);
byte data[12]; //dados recebidos do sensor
byte addr[8]; // endereço do sensor
int HighByte;
int LowByte;
int TReading;
int SignBit;
int TC_100;
double TC;

//declaraçõres restantes
double tempo = 0.0;
int contador;
double u = 0.5;
double aleatorio = 0.0;

void setup() {

  Serial.begin(115200); // inicializa a porta serial
  
  noInterrupts(); // disable all interrupts
  randomSeed(analogRead(RANDOM_PIN)); // inicializa a semente do gerador de números aleatórios com uma leitura flutuante (ruído)
  
  //define o pino da saída controlada como saída
  pinMode(PWM_OUT_PIN, OUTPUT);
  //coloca ele em alto (porque isso equivale a desligar o sistema no nosso acionamento)
  digitalWrite(PWM_OUT_PIN,HIGH);
  //Faz as definições relativas ao PWM do pino 9 (usando o timer1 do ATmega328p)
  //Fast PWM Mode com 10 bits de resolução - WGM13:0 = 7
  //como nosso acionamento é negado, queremos um PWM invertido - COM1A1:0 = 2
  //Prescalers disponíveis (N = 1 -> 001, 8 -> 010, 64 -> 011, 256 -> 100, 1024 -> 101) -> página 129 datasheet
  //Frequência do PWM = clk/(N (1 + top)), clk = 16 MHz, top = 1023, N = 1024 -> f approx 15 Hz
  //(o sistema não estava respondendo para um chaveamento mais rápido)
  //Timer/Counter Control Register 1 A
  TCCR1A = B11010011;
  //Timer/Counter Control Register 1 B
  TCCR1B = B00001101;
  
  //Verifica se consegue obter o endereço do sensor de temperatura
  if (!sensor.search(addr)) {
    Serial.print("Deu pau pra achar o endereço do sensor de temperatura");
    return;
  }
  if ( OneWire::crc8( addr, 7) != addr[7]) {
    Serial.print("CRC não é válido para o sensor de temperatura");
    return;
  }
  // Faz o reset da linha; seleciona o endereço e dá um start convertion
  sensor.reset();
  sensor.select(addr);
  sensor.write(0x44,0); //Start Conversion
  delay(750);// time needed for the adc conversion on the temperature sensor
  
  //define que o pino do LED do arduino é um pino de saída
  pinMode(LED_BUILTIN, OUTPUT);
  //acende o LED do arduino pra indicar que acabou o setup
  digitalWrite(LED_BUILTIN, HIGH);
  pwm = 0;
  interrupts(); //enables interrupts
  contador = 0;
}

void loop() {
  tempo += 0.75;
  //começa o loop fazendo a leitura do sensor de temperatura
  sensor.reset();
  sensor.select(addr);
  sensor.write(0xBE); // Read Scratchpad
    
  for (int j = 0; j < 9; j++) { // we need 9 bytes
    data[j] = sensor.read();
  }
  LowByte = data[0];
  HighByte = data[1];

  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
    
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
    TC_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25
    TC = -TC_100/100.0; 
  }
  else {
    TC_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25
    TC = TC_100/100.0;
  }
  Serial.print(tempo);
  Serial.print(" ");
  Serial.print(TC);
  Serial.print(" ");
  Serial.println(pwm);

  //faz alguma outra coisa - determina qual a entrada do sistema
  contador = contador + 1;
  //se passaram 3 minutos, 240 passos - modifica a entrada do sistema de forma aleatória
  if (contador == 240) {
      contador = 0;

      aleatorio = ((double)(random(1024) - 512))/(512.0); //gera um número aleatório entre -1 e 1, com 1024 passos entre eles
      u = 0.5 + 0.5*aleatorio; // u será um número aleatório com distribuição uniforme entre 0 e 1

      //atualiza o valor da saída de pwm a partir do duty cycle desejado
      pwm = (int)1023*u;
  }

  // Termina o loop fazendo um pedido de leitura da temperatura para o sensor
  sensor.reset();
  sensor.select(addr);
  sensor.write(0x44,0); //Start Conversion
  delay(750);// time needed for the adc conversion on the temperature sensor
}
