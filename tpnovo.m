clc;
close all;
data = load('dados_teste_peltier_agua2.mat');
t = data.dados(1,:);
y = data.dados(2,:);
u = data.dados(3,:);

delta_y = y - mean(y);
delta_u = u - mean(u);

figure();
autocorr(delta_y, 20000);
title('Auto Correlação - Dados de Treinamento');
saveas(gcf, 'AutoCorrelacaoDadosTeste.png');

yd_validacao = zeros(105, 1);
ud_validacao = zeros(105, 1);

for k = 0:104
    yd_validacao(k+1) = delta_y(k*746+1);
    ud_validacao(k+1) = delta_u(k*746+1);
end

% dados novo tp

data = load('dados_ident_peltier_agua.mat');
data = data.dados_correto(:,data.dados_correto(1,:) == 2);
t = data(2,:);
y = data(3,:);
u = data(4,:);

delta_y = y - mean(y);
delta_u = u - mean(u);

figure();
autocorr(delta_y, 99);
title('Auto Correlação - Dados de Validação');
saveas(gcf, 'AutoCorrelacaoDadosIdent.png');

figure();
crosscorr(delta_u, delta_y, 99);
title('Correlação Cruzada - Dados de Validação');
saveas(gcf, 'CorrelacaoCruzadaDadosIdent.png');

yd = zeros(100, 1);
ud = zeros(100, 1);

yd = delta_y';
ud = delta_u';

%% EStima modelo ARX com ordem mais elevada
% modelo ARX
ym = yd(3:end);
X = zeros(length(ym),3);
for i = 3:length(ym)
    X(i-2,:) = [yd(i-1) yd(i-2) ud(i-1)];
end
theta = X\ym;

% ft_aumentada = tf(theta(6:10),[1 -theta(1:5)])
% zpk(ft_aumentada)

num = theta(3)';
den = [1 -theta(1:2)'];
yd_validacao = yd;
ud_validacao = ud;
%simulacaoo um passo a frente
ym = yd_validacao(3:end);
X = zeros(length(ym),3);
for i = 3:length(ym)
    X(i-2,:) = [yd_validacao(i-1) yd_validacao(i-2) ud_validacao(i)];
end
yest = X*theta;

figure();
stairs(ym); hold on; stairs(yest,'r');
title('Comparações - Original/Simulações');
saveas(gcf, 'ComparacaoOriginalLivrePassoFrente.png');

%simulacao livre
yd_resultado_validacao = zeros(length(yd_validacao));
for n = 3:length(yd_validacao)
    yd_resultado_validacao(n) = theta(1)*yd_validacao(n-1) - theta(2)*yd_validacao(n-2) + theta(3)*ud_validacao(n-1);
end
stairs (yd_resultado_validacao,'k');
legend('original', 'passo a frente', 'livre')
saveas(gcf, 'ComparacaoOriginalLivrePassoFrente.png');

figure();
residuo = ym - yest;
autocorr(residuo, 97);
title('Auto Correlação - Residuo');
saveas(gcf,'AutoCorrelacaoResiduo.png');

figure();
crosscorr(ud_validacao(2:end), residuo);
title('Correlação Cruzada - Entrada e Residuo');
saveas(gcf,'CorrelacaoCruzadaEntradaResiduo.png');

roots(num)
roots(den)

%% Critério de Akaike (AIC)

%nÃ¯Â¿Â½mero de parÃ¯Â¿Â½metros
np = 20;
aic = zeros(np,1);

for k = 1:np
    npy = ceil(k/2);
    npx = k - npy;
    %Modelo ARX
    ym = yd((npy+1):end);
    X = zeros(length(ym),k);
    for i = (npy+1):length(ym)
        %parÃ¯Â¿Â½metros em y
        for m = 1:npy
            X(i-npy,m) = yd(i-m);
        end
        %parÃ¯Â¿Â½metros em u
        for m = 1:npx
            X(i-npy,m+npy) = ud(i-m);
        end
    end
    theta = X\ym;
    res = ym - X*theta;
    aic(k) = length(ym)*log(var(res)) + 2*k;
end

figure();
plot(1:np,aic); % o criterio deu o modelo com 4 parametros - 2a ordem
title('Akaike - AIC');
grid on;
grid minor;
saveas(gcf,'AkaikeAIC.png');