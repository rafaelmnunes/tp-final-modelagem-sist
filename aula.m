clearvars; close all;
Ts = 0.2;

%y[n] = 1.341*y[n-1] - 0.4493*y[n-2] + 0.06155*u[n-1] + 0.04714*u[n-2]
y = zeros(2002,1);
u = zeros(2002,1);
e = 0.001*randn(2002,1);

y(2) = 0;
y(1) = 0;

%u(3:end) = sin(2*pi*0.1*(3:202)*Ts);
Tb = 0.4; 
u(3:end) = randn(2000,1);

for n = 3:length(y)
    y(n) = 1.341*y(n-1) - 0.4493*y(n-2) + 0.06155*u(n-1) + 0.04714*u(n-2) + e(n);
end

figure;
stairs(Ts*(-2:1999),y); hold on;

%% 
%correla��o cruzada - xcorr porque n�o tem autocorr
[c,lags] = xcorr(y,y,100);

ca = c(lags >=0);
la = lags(lags >= 0);

figure;
stem(la,ca) % deu okfor k = 1:np

%% EStima modelo ARX com ordem mais elevada
%Modelo ARX
ym = y(6:end);
X = zeros(length(ym),10);
for i = 6:length(ym)
    X(i-5,:) = [y(i-1) y(i-2) y(i-3) y(i-4) y(i-5) u(i-1) u(i-2) u(i-3) u(i-4) u(i-5)];
end
theta = X\ym;

% ft_aumentada = tf(theta(6:10),[1 -theta(1:5)])
% zpk(ft_aumentada)

num = theta(6:10)'
den = [1 -theta(1:5)']

roots(num)
roots(den)

%deu um "cancelamento" que voltou pra segunda ordem

%% Crit�rio de Akaike (AIC)

%n�mero de par�metros
np = 20;
aic = zeros(np,1);

for k = 1:np
    npy = ceil(k/2);
    npx = k - npy;
    %Modelo ARX
    ym = y((npy+1):end);
    X = zeros(length(ym),k);
    for i = (npy+1):length(ym)
        %par�metros em y
        for m = 1:npy
            X(i-npy,m) = y(i-m);
        end
        %par�metros em u
        for m = 1:npx
            X(i-npy,m+npy) = u(i-m);
        end
    end
    theta = X\ym;
    res = ym - X*theta;
    aic(k) = length(ym)*log(var(res)) + 2*k;
end

figure;
plot(1:np,aic); % o crit�rio deu o modelo com 4 par�metros - 2� ordem


%% EStima modelo ARX com ordem certa
%Modelo ARX
ym = y(3:end);
X = zeros(length(ym),4);
for i = 3:length(ym)
    X(i-2,:) = [y(i-1) y(i-2) u(i) u(i-1)];
end
theta = X\ym;
res = ym - X*theta;

%autocorrela��o do res
[c,lags] = xcorr(res,res,100);
ca = c(lags >=0);
la = lags(lags >= 0);

figure;
stem(la,ca)

%correla��o cruzada do res com a entrada
[c,lags] = xcorr(u,res,100);
figure;
stem(lags,c);

%% Dados de valida��o
%y[n] = 1.341*y[n-1] - 0.4493*y[n-2] + 0.06155*u[n-1] + 0.04714*u[n-2]
y = zeros(2002,1);
u = zeros(2002,1);
e = 0.001*randn(2002,1);

y(2) = 0;
y(1) = 0;

%u(3:end) = sin(2*pi*0.1*(3:202)*Ts);
Tb = 0.4; 
u(3:end) = randn(2000,1);

for n = 3:length(y)
    y(n) = 1.341*y(n-1) - 0.4493*y(n-2) + 0.06155*u(n-1) + 0.04714*u(n-2) + e(n);
end

figure;
stairs(Ts*(-2:1999),y); hold on;

%simula��o um passo a frente
ym = y(3:end);
X = zeros(length(ym),4);
for i = 3:length(ym)
    X(i-2,:) = [y(i-1) y(i-2) u(i) u(i-1)];
end
yest = X*theta;

figure;
stairs(ym); hold on; stairs(yest,'r');

%simula��o livre
yl = zeros(2002,1);
yl(2) = 0;
yl(1) = 0;

for n = 3:length(yl)
    yl(n) = theta(1)*y(n-1) +theta(2)*y(n-2) + theta(3)*u(n-1) + theta(4)*u(n-2);
end
stairs (yl,'k');