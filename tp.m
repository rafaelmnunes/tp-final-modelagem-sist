data = load('dados_teste_peltier_agua2.mat');
t = data.dados(1,:);
y = data.dados(2,:);
u = data.dados(3,:);

delta_y = y - mean(y);
delta_u = u - mean(u);
autocorr(delta_y, 20000);

yd_validacao = zeros(105, 1);
ud_validacao = zeros(105, 1);

for k = 0:104
    yd_validacao(k+1) = delta_y(k*746+1);
    ud_validacao(k+1) = delta_u(k*746+1);
end
    

%%
% y = zeros(2002,1);
% u = zeros(2002,1);
% e = 0.001*randn(2002,1);
% 
% y(2) = 0;
% y(1) = 0;
% 
% %u(3:end) = sin(2*pi*0.1*(3:202)*Ts);
% Tb = 0.4; 
% u(3:end) = randn(2000,1);
% 
% for n = 3:length(y)
%     y(n) = 1.341*y(n-1) - 0.4493*y(n-2) + 0.06155*u(n-1);
% end
% 
% figure;
% stairs(Ts*(-2:1999),y); hold on;

% %% 
% %correla��o cruzada - xcorr porque n�o tem autocorr
% [c,lags] = xcorr(y,y,100);
% 
% ca = c(lags >=0);
% la = lags(lags >= 0);
% 
% figure;
% stem(la,ca) % deu okfor k = 1:np

%% EStima modelo ARX com ordem mais elevada
%Modelo ARX
ym = yd(6:end);
X = zeros(length(ym),10);
for i = 6:length(ym)
    X(i-5,:) = [yd(i-1) yd(i-2) yd(i-3) yd(i-4) yd(i-5) ud(i-1) ud(i-2) ud(i-3) ud(i-4) ud(i-5)];
end
theta = X\ym;

% ft_aumentada = tf(theta(6:10),[1 -theta(1:5)])
% zpk(ft_aumentada)

num = theta(6:10)';
den = [1 -theta(1:5)'];

roots(num)
roots(den)

%%

%% Crit�rio de Akaike (AIC)

%n�mero de par�metros
np = 20;
aic = zeros(np,1);

for k = 1:np
    npy = ceil(k/2);
    npx = k - npy;
    %Modelo ARX
    ym = yd((npy+1):end);
    X = zeros(length(ym),k);
    for i = (npy+1):length(ym)
        %par�metros em y
        for m = 1:npy
            X(i-npy,m) = yd(i-m);
        end
        %par�metros em u
        for m = 1:npx
            X(i-npy,m+npy) = ud(i-m);
        end
    end
    theta = X\ym;
    res = ym - X*theta;
    aic(k) = length(ym)*log(var(res)) + 2*k;
end

figure;
plot(1:np,aic); % o crit�rio deu o modelo com 4 par�metros - 2� ordem
